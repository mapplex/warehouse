#warehouse仓库管理

# 一、部署说明 #
> 1. 创建mysql数据，运行sql/mysql.sql数据库文件（mysql_by_data.sql这个是带测试数据的）。
> 2. 修改src/conf/db.properties文件，根据自己的数据配置，修改数据名、用户名、密码。
> 3. 这时候就可以启动项目了~！~自己测试吧，默认账号：admin 密码：123456
> 4. 部署环境：mysql5.6及以上版本，jdk6及以上版本，tomcat6及以上版本
> 5. 开发环境：eclipse3.6及以上版本，本人开发环境eclipse4.4。

# 二、结构说明 #
> 1.  src为源文件，conf下为配置文件 
config.properties这个文件尽量不要修改，会导致产生错误。
mysql.properties数据配置
> 2. com.flyfox 为根目录；
com.flyfox.util 为工具类
com.flyfox.component.hibernate 为hibernate底层封装
com.flyfox.component.spring 为spring底层封装
com.flyfox.modules 为各个模块开发；
com.flyfox.modules.dict 系统管理；
com.flyfox.modules.user 用户管理模块；
com.flyfox.modules.goods 物资管理、出入库管理和盘库管理；
com.flyfox.modules.commom 为登陆、登陆等
> 3. sql目录mysql.sql无数据初始化脚本，mysql_by_data.sql有数据初始化脚本。
> 4. WebRoot下，pages为所有增删改查页面，static为第三方库、css和图片。
> 5. WebRoot/WEB-INF/lib为java第三方jar，web.xml为jfinal拦截器配置，login.jsp登陆页面。

# 三、其他说明 #
> 1. 以用户管理为例分为controller、model、service、dao以及前台jsp页面
controller使用springmvc注解、model使用hibernate注解映射数据库
service使用spring注解注入bean，dao使用spring注解注入bean
前台jsp页面基本分为增加add，修改edit，列表list、查看view
> 2. freemarker/user/pwd.ftl 为freemarker视图 密码修改
> 3. velocity/goodsdetail/list_check.vm 为velocity视图 盘库明细
